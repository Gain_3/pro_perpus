<?php
	require "../inc/penting.php";

	$sql		= "SELECT * FROM `distributor`";
	$eksekusi	= $koneksi->query($sql);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Distributor</title>
</head>
<body>
	<table border='1'>
		<thead>
			<tr>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Telpon</th>
				<th>Email</th>
				<th colspan="2">Aksi</th>
			</tr>
		</thead>
		<?php
			while($data = $eksekusi->fetch(PDO::FETCH_OBJ)){
		?>

		<tbody>
			<tr>
				<td><?php echo $data->nama_distributor; ?></td>
				<td><?php echo $data->alamat_distributor; ?></td>
				<td><?php echo $data->no_telpon; ?></td>
				<td><?php echo $data->email; ?></td>
				<td><a href="hapus_distributor.php?id=<?php echo $data->id_distributor?>">Hapus</a></td>
				<td><a href="form_ubah_distributor.php?id=<?php echo $data->id_distributor?>">Ubah</a></td>
			</tr>
		</tbody>

		<?php
			}
		?>
	</table>
</body>
</html>