<?php
	require '../inc/penting.php';

	$id		= $_GET['id'];

	$sql		= "SELECT * FROM `user` WHERE `id_user`='$id'";
	$eksekusi	= $koneksi->prepare($sql);	
	$eksekusi->execute();	

	$data 		= $eksekusi->fetch(PDO::FETCH_OBJ);
?>

<!DOCTYPE html>
<html>
<head>
	<title>User</title>
</head>
<body>
	<h1>User</h1>

	<form method="post" action="proses_input_user.php" enctype="multipart/form-data">
		<table>
			<input type="hidden" name="id" value="<?php echo $data->id_user;?>">
			<tr>
				<td>No Identitas</td>
				<td>:</td>
				<td><input type="text" name="no_identitas" value="<?php echo $data->no_identitas;?>"></td>
			</tr>
			<tr>
				<td>No KK</td>
				<td>:</td>
				<td><input type="text" name="no_kk" value="<?php echo $data->no_kk;?>"></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>:</td>
				<td><input type="text" name="nama" value="<?php echo $data->nama_user;?>"></td>
			</tr>
			<tr>
				<td>Tempat Lahir</td>
				<td>:</td>
				<td><input type="text" name="tempat" value="<?php echo $data->tempat_lahir;?>"></td>
			</tr>
			<tr>
				<td>Tanggal Lahir</td>
				<td>:</td>
				<td><input type="date" name="tanggal" value="<?php echo $data->tanggal_lahir;?>"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td>
					<textarea name="alamat_user"><?php echo $data->alamat;?></textarea>
				</td>
			</tr>
			<tr>
				<td>Telpon</td>
				<td>:</td>
				<td><input type="text" name="telp_user" value="<?php echo $data->no_telpon;?>"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td>:</td>
				<td><input type="text" name="email" value="<?php echo $data->email;?>"></td>
			</tr>
			<tr>
				<td>Photo</td>
				<td>:</td>
				<td><input type="file" name="photo" value="<?php echo $data->photo;?>"></td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td><input type="submit" name="simpan" value="Simpan"></td>
			</tr>
		</table>
	</form>
	<br>
</body>
</html>

<?php
	require '../inc/penting.php';

	require 'tampil_user.php';
?>