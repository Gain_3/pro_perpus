<?php
	require "../inc/penting.php";

	$sql		= "SELECT * FROM `jenis_kebutuhan`";
	$eksekusi	= $koneksi->query($sql);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Jenis Kebutuhan</title>
</head>
<body>
	<table border='1'>
		<thead>
			<tr>
				<th>Jenis Kebutuhan</th>
				<th colspan="2">Aksi</th>
			</tr>
		</thead>
		<?php
			while($data = $eksekusi->fetch(PDO::FETCH_OBJ)){
		?>

		<tbody>
			<tr>
				<td><?php echo $data->nama_jenis_kebutuhan; ?></td>
				<td><a href="hapus_jenis_kebutuhan.php?id=<?php echo $data->id_jenis_kebutuhan?>">Hapus</a></td>
				<td><a href="form_ubah_jenis_kebutuhan.php?id=<?php echo $data->id_jenis_kebutuhan?>">Ubah</a></td>
			</tr>
		</tbody>

		<?php
			}
		?>
	</table>
</body>
</html>